//
//  ViewController.swift
//  RSSFeedsReader
//
//  Created by SyLar on 2018/6/11.
//  Copyright © 2018 Ken. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "RSS feeds reader"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


//
//  NewsFeedViewController.swift
//  RSSFeedsReader
//
//  Created by SyLar on 2018/6/11.
//  Copyright © 2018 Ken. All rights reserved.
//

import UIKit
import SafariServices

class NewsFeedViewController: UIViewController {
    @IBOutlet weak var newsFeedTableView: UITableView!

    private var newsFeeds = [NewsFeed]()
    private var newsFeedXMLParser: NewsFeedXMLParser?

    private var spinnerView: UIView?

    override func viewDidLoad() {
        newsFeedXMLParser = NewsFeedXMLParser.init()
        newsFeedXMLParser?.delegate = self

        newsFeedTableView.delegate = self
        newsFeedTableView.dataSource = self

        if let newsFeedXMLParser = newsFeedXMLParser {
            title = newsFeedXMLParser.rssFeedUrl.host

            newsFeedXMLParser.parse()
        }

        spinnerView = UIViewController.displaySpinner(onView : self.view)
    }
}

//REMARK: UITableViewDataSource
extension NewsFeedViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsFeeds.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NewsFeedCell.cellIdentifier, for: indexPath)
        let newsFeedCell = cell as? NewsFeedCell
        newsFeedCell?.layout(with: newsFeeds[indexPath.row])
        return cell
    }

}

//REMARK: UITableViewDelegate
extension NewsFeedViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newsFeed = newsFeeds[indexPath.row]
        let safari = SFSafariViewController(url: newsFeed.link)
        if #available(iOS 11.0, *) {
            safari.navigationItem.largeTitleDisplayMode = .never
        } else {
            // Fallback on earlier versions
        }
        present(safari, animated: true, completion: nil)
    }
}

//REMARK: NewsFeedXMLParserDelegate
extension NewsFeedViewController: NewsFeedXMLParserDelegate {
    func didFinishParsingWith(parsedResult: [String: Any]?) {
        guard let channel = parsedResult?["channel"] as? [String: Any],
            let items = channel["item"] as? [[String: Any]] else {
                print("[ERROR] No valid element is parsed")
                return
        }
        let newsFeeds = items.compactMap(dictionaryToNewsFeed)
        if self.newsFeeds.isEmpty {
            self.newsFeeds = newsFeeds
        } else {
            self.newsFeeds.append(contentsOf: newsFeeds)
        }
        DispatchQueue.main.async { [unowned self] in
            self.newsFeedTableView?.reloadData()

            if let spinnerView = self.spinnerView {
                UIViewController.removeSpinner(spinner: spinnerView)
            }
        }
    }
}

//
//  NewsFeedCell.swift
//  RSSFeedsReader
//
//  Created by SyLar on 2018/6/11.
//  Copyright © 2018 Ken. All rights reserved.
//

import UIKit

class NewsFeedCell: UITableViewCell {
    static var cellIdentifier: String {
        return "NewsFeedCell"
    }
    @IBOutlet weak var thumbnailImageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    func layout(with newsFeed: NewsFeed) {
        titleLabel.text = newsFeed.title
        descriptionLabel.text = newsFeed.description
        ImageDownloader.shared.fetchImage(with: newsFeed) { (image) in
            DispatchQueue.main.async { [unowned self] in
                self.thumbnailImageView?.image = image
            }
        }
    }

}

//
//  ImageDownloader.swift
//  RSSFeedsReader
//
//  Created by SyLar on 2018/6/12.
//  Copyright © 2018 Ken. All rights reserved.
//

import UIKit

class ImageDownloader: NSObject {
    static let shared = ImageDownloader(cacheLimit: 20)

    private var imageCache: NSCache<NSString, UIImage>!

    //REMARK: Init
    @available(*, unavailable)
    override init() {
        fatalError()
    }

    private init(cacheLimit: Int) {
        self.imageCache = NSCache()
        self.imageCache.countLimit = cacheLimit
        super.init()
    }

    //REMARK: Public
    func fetchImage(with newsFeed:NewsFeed, completion: @escaping (UIImage?) -> ()) {
        if let image = imageCache.object(forKey: newsFeed.guid as NSString) {
            return completion(image)
        }
        URLSession.shared.downloadTask(with: newsFeed.imageUrl) { (url, response, error) in
            if let url = url, let data = try? Data(contentsOf: url), let image = UIImage(data: data) {
                self.imageCache.setObject(image, forKey: newsFeed.guid as NSString)
                return completion(image)
            }
            if let response = response as? HTTPURLResponse {
                print("[ERROR] Failed to download the image with code: \(response.statusCode), error: \(String(describing: error?.localizedDescription))")
            }
            }.resume()
    }
}

//
//  NewsFeed.swift
//  RSSFeedsReader
//
//  Created by SyLar on 2018/6/11.
//  Copyright © 2018 Ken. All rights reserved.
//

import UIKit

/**
 <item>
 <title>Ireland and U.K. Maneuver on Border Question</title>
 <guid isPermaLink="false">SB11628054395813373602304584272440911239558</guid>
 <link>
 https://www.wsj.com/articles/ireland-and-u-k-maneuver-on-border-question-1528552800?mod=fox_australian
 </link>
 <description>
 As talks between the European Union and the U.K. on Brexit enter a critical phase, the leaders of Ireland and Britain are in very different form. One knows where he is going, Paul Hannon writes; the other still can’t be sure.
 </description>
 <media:content xmlns:media="http://search.yahoo.com/mrss" url="http://s.wsj.net/public/resources/images/S1-AM599_0608br_G_20180608160137.jpg" type="image/jpeg" medium="image" height="369" width="553">
 <media:description>image</media:description>
 </media:content>
 <category>FREE</category>
 <pubDate>Sat, 09 Jun 2018 10:00:04 EDT</pubDate>
 </item>
 */

struct NewsFeed {
    var guid: String

    var title: String
    var link: URL
    var description: String

    var imageUrl: URL
    var imageSize: CGSize
}

func dictionaryToNewsFeed(dict: [String: Any]) -> NewsFeed? {
    guard let guidDict = dict["guid"] as? [String: Any],
        let guid = guidDict["content"] as? String else {
            return nil
    }

    guard let titleDict = dict["title"] as? [String: Any],
        let title = titleDict["content"] as? String else {
            return nil
    }

    guard let linkDict = dict["link"] as? [String: Any],
        let linkString = linkDict["content"] as? String,
        let link = URL.init(string:linkString) else {
            return nil
    }

    guard let descriptionDict = dict["description"] as? [String: Any],
        let description = descriptionDict["content"] as? String else {
            return nil
    }

    guard let imageDict = dict["media:content"] as? [String: Any],
        let imageAttrs = imageDict["ATTR"] as? [String: Any],
        let imageUrlString = imageAttrs["url"] as? String,
        let imageUrl = URL.init(string:imageUrlString) else {
            return nil
    }

    guard let widthString = imageAttrs["width"] as? String,
        let heightString = imageAttrs["height"] as? String,
        let width = Int(widthString),
        let height = Int(heightString)  else {
            return nil
    }
    let imageSize = CGSize(width: width, height: height)

    return NewsFeed(guid: guid, title: title, link: link, description: description, imageUrl: imageUrl, imageSize: imageSize)
}

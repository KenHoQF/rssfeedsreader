//
//  NewsFeedXMLParser.swift
//  RSSFeedsReader
//
//  Created by SyLar on 2018/6/11.
//  Copyright © 2018 Ken. All rights reserved.
//

import Foundation

protocol NewsFeedXMLParserDelegate: class {
    func didFinishParsingWith(parsedResult: [String: Any]?)
}

class NewsFeedXMLParser: NSObject {
    //REMARK: Public para
    var delegate: NewsFeedXMLParserDelegate?
    var rssFeedUrl: URL!

    //REMARK: Private para
    private var xmlParser: XMLParser!
    private let parserQueue: DispatchQueue!
    private var parsedContent = [[String: Any]]()
    private var indexPath = [String]()

    //REMARK: Init
    @available(*, unavailable)
    override init() {
        fatalError()
    }

    init(rssFeedUrl: URL) {
        parserQueue = DispatchQueue(label: "RSSFeedsParser")
        self.rssFeedUrl = rssFeedUrl
        super.init()
        parserQueue.async { [unowned self] in
            self.xmlParser = XMLParser.init(contentsOf: rssFeedUrl)
            self.xmlParser.delegate = self
        }
    }

    convenience init?(rssFeedSource: String = "http://www.wsj.com/xml/rss/3_7085.xml") {
        guard let rssFeedUrl = URL.init(string: rssFeedSource) else {
            print("[ERROR] Incorrect rss feed source: \(rssFeedSource)")
            return nil
        }
        self.init(rssFeedUrl: rssFeedUrl)
    }

    //REMARK: Parser
    func parse() {
        parserQueue.async { [unowned self] in
            self.xmlParser.parse()
        }
    }

    func abortParsing() {
        self.xmlParser.abortParsing()
    }
}

//REMARK: XMLParserDelegate
extension NewsFeedXMLParser: XMLParserDelegate {
    func parserDidStartDocument(_ parser: XMLParser) {
        print("[INFO] \(#function)")
    }
    func parserDidEndDocument(_ parser: XMLParser) {
        print("[INFO] \(#function)")
        delegate?.didFinishParsingWith(parsedResult: parsedContent.first)
    }

    func parser(_ parser: XMLParser, foundCharacters string: String) {
        guard var lastContent = parsedContent.popLast() else {
            return
        }
        if let existingString = lastContent["content"] as? String {
            lastContent["content"] = existingString + string
        } else {
            lastContent["content"] = string
        }
        parsedContent.append(lastContent)
    }

    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        guard let lastPathComponent = indexPath.popLast(),
            let lastContent = parsedContent.popLast() else {
                return
        }
        guard var previousContent = parsedContent.popLast() else {
            parsedContent.append(lastContent)
            return
        }
        if var children = previousContent[lastPathComponent] as? [Any] {
            children.append(lastContent)
            previousContent[lastPathComponent] = children
        } else if let child = previousContent[lastPathComponent] {
            previousContent[lastPathComponent] = [child, lastContent]
        } else {
            previousContent[lastPathComponent] = lastContent
        }
        parsedContent.append(previousContent)
    }

    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        indexPath.append(elementName)
        parsedContent.append(["ATTR": attributeDict])
    }
}
